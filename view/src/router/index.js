import VueRouter from 'vue-router';
import UnloggedNavbar from '@/components/navbar/UnloggedNavbar.vue';
import HomePage from '@/components/HomePage.vue';
import LoggedNavbar from '@/components/navbar/LoggedNavbar.vue';
import LicensePage from '@/components/docs/License.vue';
import UserSidebar from '@/components/Sidebar.vue';
import Home from '@/view/Home.vue';
import MyPosts from '@/view/MyPosts.vue';
import MyGallery from '@/view/Gallery.vue';
import MyFriends from '@/view/Friends.vue';
import PrivacyPolicy from '@/components/docs/PrivacyPolicy.vue';
import TermsOfUse from '@/components/docs/TermsOfUse.vue';
import BlockedUsers from '@/view/BlockedUsers.vue';
import SearchResults from '@/view/SearchResults.vue';
import PendingFriendRequests from '@/view/PendingFriendRequests.vue';
import ChatWindow from '@/components/ChatWindow.vue';
import FriendProfile from '@/view/FriendProfile.vue';
import MutualFriends from '@/view/MutualFriends.vue';

const routes = [
  {
    path: '/',
    components: {
      Navbar: UnloggedNavbar,
      UnloggedContent: HomePage
    }
  },
  {
    path: '/license',
    components: {
      Navbar: UnloggedNavbar,
      UnloggedContent: LicensePage
    }
  },
  {
    path: '/home',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: Home
    }
  },
  {
    path: '/user/license',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: LicensePage
    }
  },
  {
    path: '/myposts',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: MyPosts
    }
  },
  {
    path: '/gallery',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: MyGallery
    }
  },
  {
    path: '/friends',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: MyFriends
    }
  },
  {
    path: '/privacypolicy',
    components: {
      Navbar: UnloggedNavbar,
      UnloggedContent: PrivacyPolicy
    }
  },
  {
    path: '/termsofuse',
    components: {
      Navbar: UnloggedNavbar,
      UnloggedContent: TermsOfUse
    }
  },
  {
    path: '/user/privacypolicy',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: PrivacyPolicy
    }
  },
  {
    path: '/user/termsofuse',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: TermsOfUse
    }
  },
  {
    path: '/blockedusers',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: BlockedUsers
    }
  },
  {
    path: '/results',
    components: {
      Navbar: UnloggedNavbar,
      UnloggedContent: SearchResults
    }
  },
  {
    path: '/user/results',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: SearchResults
    }
  },
  {
    path: '/pendingRequests',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: PendingFriendRequests
    }
  },
  {
    path: '/chat/t/:chatid',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: ChatWindow
    }
  },
  {
    path: '/friend/info/:id',
    name: 'friendInfo',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: FriendProfile
    }
  },
  {
    path: '/unloggedviewer/user/info/:id',
    name: 'unloggedUserView',
    components: {
      Navbar: UnloggedNavbar,
      UnloggedContent: FriendProfile
    }
  },
  {
    path: '/user/mutual-friends/:personid',
    components: {
      Navbar: LoggedNavbar,
      LeftSidebar: UserSidebar,
      MainContent: MutualFriends
    }
  }
];

const router = new VueRouter({
  mode: 'hash',
  base: __dirname,
  routes
});


export default router;