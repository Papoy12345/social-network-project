package model;

import java.util.Date;
import java.util.List;

import model.enums.Gender;
import model.enums.Role;

public class User {
	private Integer id;
	private Role role;
	private Gender gender;
	private String username;
	private String password;
	private String name;
	private String surname;
	private Date dateOfBirth;
	private String profilePicture;
	private String email;
	
	private Boolean activated;
	private Boolean suspended;
	private Boolean privated;
	
	private List<Integer> friends; // user.id
	private List<Integer> blocked; // user.id
	private List<Integer> friendRequests; // friendrequest.id
	private List<Integer> posts; // post.id
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	public Boolean getActivated() {
		return activated;
	}
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}
	public Boolean getSuspended() {
		return suspended;
	}
	public void setSuspended(Boolean suspended) {
		this.suspended = suspended;
	}
	public Boolean getPrivated() {
		return privated;
	}
	public void setPrivated(Boolean privated) {
		this.privated = privated;
	}
	public List<Integer> getFriends() {
		return friends;
	}
	public void setFriends(List<Integer> friends) {
		this.friends = friends;
	}
	public List<Integer> getBlocked() {
		return blocked;
	}
	public void setBlocked(List<Integer> blocked) {
		this.blocked = blocked;
	}
	public List<Integer> getFriendRequests() {
		return friendRequests;
	}
	public void setFriendRequests(List<Integer> friendRequests) {
		this.friendRequests = friendRequests;
	}
	public List<Integer> getPosts() {
		return posts;
	}
	public void setPosts(List<Integer> posts) {
		this.posts = posts;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public void blockUser(Integer blockid) {
		friends.remove(blockid);
		blocked.add(blockid);
	}
	public void unblockUser(Integer blockedid) {
		blocked.remove(blockedid);
		
	}
	
	public String getFullName() {
		return getName() + " " + getSurname();
	}
	
}
