package util;

import java.security.Key;
import java.util.Date;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import model.User;
import model.enums.Role;
import spark.Request;

public class Authorizer {
	private static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

	public String createJWTToken(User u, Request req) {
		String jws = "Bearer " + Jwts.builder().setSubject(u.getUsername()).setHeaderParam("Role", u.getRole())
				.setExpiration(new Date(new Date().getTime() + 1000 * 3600L)).setIssuedAt(new Date()).signWith(key)
				.compact();
		System.out.println(jws);
		return jws;
	}

	// PARAMS:
	// req - http request
	// r - which role user needs to have in order to make the request
	// ADMIN can do anything
	// if GUEST tries to do ADMIN operations, returns false
	public boolean checkJWTToken(Request req, Role r) {
		String auth = req.headers("Authorization");
		if (null != auth && auth.contains("Bearer ")) {
			String jwt = auth.substring(auth.indexOf("Bearer ") + 7);
			try {
				Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwt);

				JwsHeader<?> h = claims.getHeader();
				String role = (String) h.get("Role");

				if (role.equals(Role.ADMIN.toString())) {
					return true;
				}
				if (!role.equals(r.toString())) {
					return false;
				}
				// IF THERE ARE NO PARSING ERRORS
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}
}
