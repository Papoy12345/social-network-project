package util;

import java.io.FileNotFoundException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import controllers.ChatController;
import controllers.CommentController;
import controllers.FriendRequestController;
import controllers.PictureController;
import controllers.PostController;
import controllers.SearchController;
import controllers.UserController;
import repositories.CommentRepository;
import repositories.FriendRequestRepository;
import repositories.PictureRepository;
import repositories.PostRepository;
import repositories.UserRepository;
import services.ChatService;
import services.CommentService;
import services.FriendRequestService;
import services.PictureService;
import services.PostService;
import services.SearchService;
import services.UserService;

public class Injector {
	private Gson gson;
	private Authorizer auth;
	
	public CommentRepository cr;
	public FriendRequestRepository frr;
	public PictureRepository picr;
	public PostRepository posr;
	public UserRepository ur;
	
	public CommentService cs;
	public FriendRequestService frs;
	public PictureService pics;
	public PostService poss;
	public UserService us;
	public SearchService ss;
	public ChatService chs;
	
	public CommentController cc;
	public FriendRequestController frc;
	public PictureController picc;
	public PostController posc;
	public UserController uc;
	public SearchController sc;
	public ChatController chc;
	
	
	public Injector() {
		gson = new GsonBuilder().create();
		auth = new Authorizer();
	}
	
	public void initiate() throws FileNotFoundException {
		initiateRepositories();
		initiateServices();
		initiateControllers();
	}

	private void initiateControllers() {
		cc = new CommentController(cs, gson, auth);
		frc = new FriendRequestController(frs, gson, auth);
		picc = new PictureController(pics, gson, auth);
		posc = new PostController(poss, gson, auth);
		uc = new UserController(us, gson, auth);
		sc = new SearchController(ss, gson);
		chc = new ChatController(chs, auth);
	}

	private void initiateServices() {
		cs = new CommentService(cr, ur, posr);
		pics = new PictureService(picr, ur, cr);
		poss = new PostService(posr, ur, cr);
		us = new UserService(ur);
		ss = new SearchService(ur);
		frs = new FriendRequestService(frr, us);
		chs = new ChatService(ur);
	}

	private void initiateRepositories() throws FileNotFoundException {
		cr = new CommentRepository();
		frr = new FriendRequestRepository();
		picr = new PictureRepository();
		posr = new PostRepository();
		ur = new UserRepository();
	}
	
	

}
