package services;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonIOException;

import javaxt.http.Request;
import model.FriendRequest;
import model.enums.Status;
import repositories.FriendRequestRepository;
import repositories.UserRepository;

public class FriendRequestService {
	
	private FriendRequestRepository repo;
	private UserService userService;
	
	public FriendRequestService(FriendRequestRepository frr, UserService us) {
		repo = frr;
		userService = us;
	}
	
	public List<FriendRequest> getPendingRequests(Integer id){
		return repo.getByReceiver(id,Status.PENDING);
	}
	
	public void acceptRequest(Integer id) throws JsonIOException, IOException {
		repo.changeStatus(id, Status.ACCEPTED);
		FriendRequest r = repo.getById(id);
		userService.addFriend(r.getReceiver(), r.getSender());
		repo.updateRequests();
	}
	
	public void denyRequest(Integer id) throws JsonIOException, IOException {
		repo.changeStatus(id, Status.DENIED);
		repo.updateRequests();
	}
	
	public void sendRequest(Integer sender, Integer receiver) throws JsonIOException, IOException {
		FriendRequest request = new FriendRequest();
		request.setId(repo.getNewId());
		request.setReceiver(receiver);
		request.setSender(sender);
		request.setStatus(Status.PENDING);
		request.setDate(new Date());
		
		repo.addRequest(request);
	}

}