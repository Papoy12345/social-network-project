package services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.JsonIOException;

import model.User;
import model.enums.Gender;
import model.enums.Role;
import repositories.UserRepository;

public class UserService {
	private UserRepository repo;
	
	public UserService(UserRepository ur) {
		repo = ur;
	}
	
	public void addFriend(Integer receiver, Integer sender) throws JsonIOException, IOException {
		repo.addFriend(receiver,sender);
	}

	public User attemptLogin(String emailOrUsername, String password) {
		User retval = repo.getByEmail(emailOrUsername);
		if (null == retval) {
			retval = repo.getByUsername(emailOrUsername);
		}
		if (null != retval) {
			retval = password.equals(retval.getPassword()) ? retval : null; 
		}
		return retval;
	}
	
	public boolean checkUniqueCredentials(String username, String email) {
		User attempt1 = repo.getByEmail(email);
		User attempt2 = repo.getByUsername(username);
		
		return null == attempt1 && null == attempt2;
	}
	
	public boolean checkNewPassword(String password1, String password2) {
		return password1.equals(password2) && password1.length() >= 8;
	}
	
	public void registerUser
	(String username, String password, String email,
	 String name, String surname, Date dateOfBirth, Gender gender) throws JsonIOException, IOException {
		User u = new User();
		u.setUsername(username);
		u.setPassword(password);
		u.setEmail(email);
		u.setName(name);
		u.setSurname(surname);
		u.setGender(gender);
		u.setDateOfBirth(dateOfBirth);
		switch (gender) {
		case MALE: { u.setProfilePicture("http://localhost:8080/images/avatars/male_avatar.jpg"); break; }
		case FEMALE: { u.setProfilePicture("http://localhost:8080/images/avatars/female_avatar.jpg"); break; }
		default: { return; }
		}
		
		u.setActivated(true);
		u.setSuspended(false);
		u.setPrivated(false);
		u.setBlocked(new ArrayList<Integer>());
		u.setFriends(new ArrayList<Integer>());
		u.setFriendRequests(new ArrayList<Integer>());
		u.setPosts(new ArrayList<Integer>());
		u.setRole(Role.GUEST);
		
		this.repo.registerUser(u);
		
	}
	
	public void removeFriend(Integer user, Integer friendId) throws JsonIOException, IOException {
		repo.removeFriend(user, friendId);
	}

	public List<User> getFriends(Integer userid) {
		User user = repo.getById(userid);
		List<User> retval = new ArrayList<User>(user.getFriends().size());
		for (Integer i : user.getFriends()) {
			retval.add(repo.getById(i));
		}
		return retval;
	}
	
	public void suspendUser(Integer usrid) throws JsonIOException, IOException {
		repo.suspendUser(usrid);
	}

	public void blockPerson(Integer userid, Integer blockid) throws Exception {
		User u = repo.getById(userid);
		User blocked = repo.getById(blockid);
		if (null == blocked || null == u) {
			throw new Exception("Non-existing ids!");
		}
		u.blockUser(blockid);
		repo.updateUsers();
		
		
	}

	public void changePassword(Integer userid, String oldPassword, String newPassword1, String newPassword2) throws Exception {
		User u = repo.getById(userid);
		if (u.getPassword().equals(oldPassword) && checkNewPassword(newPassword1, newPassword2) && !oldPassword.equals(newPassword1)) {
			u.setPassword(newPassword1);
			repo.updateUsers();
		} else {
			throw new Exception("Update failed");
		}
	}
	
	public User changeProfilePicture(Integer userid, String imgpath) throws Exception {
		User u = repo.getById(userid);
		if (null != u) {
			u.setProfilePicture(imgpath);
			repo.updateUsers();
			return u;
		} else {
			throw new Exception("User doesnt exist!");
		}
		
	}

	public List<User> getBlockedUsers(Integer userid) {
		User user = repo.getById(userid);
		List<User> retval = new ArrayList<User>(user.getBlocked().size());
		for (Integer i : user.getBlocked()) {
			retval.add(repo.getById(i));
		}
		return retval;
	}

	public void unblockPerson(Integer userid, Integer blockedid) throws Exception {
		User u = repo.getById(userid);
		User blocked = repo.getById(blockedid);
		if (null == blocked || null == u) {
			throw new Exception("Non-existing ids!");
		}
		u.unblockUser(blockedid);
		repo.updateUsers();
		
		
	}

	public User getUser(Integer personid) {
		return repo.getById(personid);
	}
	
	public void togglePrivate(Integer id, Boolean priv) throws JsonIOException, IOException {
		repo.togglePrivate(id, priv);
	}
	
	public List<User> getMutualFriends(Integer user, Integer friend) {
		List<User> result = new ArrayList<User>();
		for(User u : repo.getActiveUsers()) {
			if(u.getFriends().contains(user) && u.getFriends().contains(friend)) {
				result.add(u);
			}
		}
		return result;
	}
	
}
