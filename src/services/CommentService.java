package services;

import java.io.IOException;
import java.util.Date;

import com.google.gson.JsonIOException;

import model.Comment;
import model.Post;
import model.User;
import repositories.CommentRepository;
import repositories.PostRepository;
import repositories.UserRepository;

public class CommentService {
	
	private CommentRepository repo;
	private UserRepository ur;
	private PostRepository pr;
	
	public CommentService(CommentRepository cr, UserRepository ur, PostRepository pr) {
		repo = cr;
		this.ur = ur;
		this.pr = pr;
	}
	
	// TODO: REFACTOR
	public Comment postComment(Integer posterid, String comment, Integer postid) throws JsonIOException, IOException {
		Comment newComment = new Comment();
		newComment.setId(generateCommentId());
		newComment.setComment(comment);
		newComment.setPosterId(posterid);
		newComment.setDate(new Date());
		newComment.setEdited(false);
		newComment.setDeleted(false);
		repo.createComment(newComment);
		
		Post p = pr.getById(postid);
		p.getCommentsIds().add(newComment.getId());
		pr.updatePosts();
		
		User u = this.ur.getById(posterid);
		newComment.setPoster(u);
		return newComment;
		
	}
	
	private Integer generateCommentId() {
		return repo.getNewId();
	}
	
	public void deleteComment(Integer comid) throws JsonIOException, IOException {
		repo.deleteComment(comid);
	}
	
	public void editComment(String newText, Integer comId, Integer userId) throws Exception {
		User u = ur.getById(userId);
		Comment c = repo.getById(comId);
		if (c.getPosterId() != u.getId()) {
			throw new Exception("Invalid user!");
		}
		c.setComment(newText);
		c.setEdited(true);
		repo.updateComments();
	}
	
}
