package services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.User;
import model.enums.SortOption;
import repositories.UserRepository;

public class SearchService {
	private UserRepository ur;

	public SearchService(UserRepository ur) {
		this.ur = ur;
	}

	public List<User> combineSearch(String nameQuery, Date[] dateRange) {
		List<User> result = new ArrayList<User>();
		
		if (nameQuery != null) 
			result.addAll(searchByName(nameQuery));
	    else 
			result = ur.getActiveUsers();
		
		if (checkDates(dateRange))
			result.removeAll(searchByBithRange(dateRange[0], dateRange[1], result));

		if (nameQuery.isBlank() && dateRange.length == 0)
			return ur.getActiveUsers();
		return result;
	}

	private Boolean checkDates(Date[] dateRange) {
		if (dateRange[0] == null)
			dateRange[0] = new Date(Integer.MIN_VALUE);
		if (dateRange[1] == null)
			dateRange[1] = new Date();
		if (dateRange[0].before(dateRange[1]))
			return true;
		return false;

	}

	public List<User> searchByName(String query) {
		List<User> result = new ArrayList<User>();
		for (User u : ur.getActiveUsers()) {
			if (u.getFullName().toLowerCase().contains(query.toLowerCase())) {
				result.add(u);
			}
		}

		return result;
	}

	public List<User> searchByBithRange(Date min, Date max, List<User> list) {
		List<User> result = new ArrayList<User>();
		for (User u : list) {
			if (u.getDateOfBirth().before(min) && u.getDateOfBirth().after(max)) {
				result.add(u);
			}
		}

		return result;
	}

	public List<User> sortBy(List<User> list, SortOption option) {
		List<User> result = new ArrayList<User>();
		switch (option) {
		case NAME: {
			list.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
			break;
		}
		case SURNAME: {
			list.sort((o1, o2) -> o1.getSurname().compareTo(o2.getSurname()));
			break;
		}
		case DOB: {
			list.sort((o1, o2) -> o1.getDateOfBirth().compareTo(o2.getDateOfBirth()));
			break;
		}
		}
		return result;
	}

	public List<User> searchByEmail(String email) {
		List<User> result = new ArrayList<User>();
		for(User u : ur.getActiveUsers()) {
			if(u.getEmail().contains(email)) {
				result.add(u);
			}
		}
		return result;
	}

}
