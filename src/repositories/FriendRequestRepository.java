package repositories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonIOException;

import model.FriendRequest;
import model.enums.Status;
import util.DataHandler;

public class FriendRequestRepository {
	
	private Map<Integer, FriendRequest> friendRequests;
	
	public FriendRequestRepository() throws FileNotFoundException {
			friendRequests = new HashMap<Integer,FriendRequest>();
			
			for (FriendRequest c : DataHandler.loadFriendRequests()) {
				friendRequests.put(c.getId(), c);
			}
		}
		
		public FriendRequest getById(Integer id) {
			return friendRequests.get(id);
		}
		
		public List<FriendRequest> getByReceiver(Integer id, Status status){
			List<FriendRequest> r = new ArrayList<FriendRequest>();
			for(FriendRequest fr : friendRequests.values()) {
				if (fr.getReceiver().equals(id) && status.equals(fr.getStatus())) {
						r.add(fr);
				}
				
			}
			return r;
		}
		
		public void changeStatus(Integer id, Status newStatus) {
			friendRequests.get(id).setStatus(newStatus);
		}
		
		public void addRequest(FriendRequest fr) throws JsonIOException, IOException {
			friendRequests.put(fr.getId(), fr);
			DataHandler.writeFriendRequests(new ArrayList<FriendRequest>(friendRequests.values()));
		}
		
		public Integer getNewId() {
			return friendRequests.size() + 1;
		}
		
		public void updateRequests() throws JsonIOException, IOException {
			DataHandler.writeFriendRequests(new ArrayList<FriendRequest>(friendRequests.values()));
		}
	
}
