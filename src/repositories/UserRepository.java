package repositories;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonIOException;

import model.User;
import util.DataHandler;

public class UserRepository {

	private Map<Integer, User> users;

	public UserRepository() throws FileNotFoundException {
		users = new HashMap<Integer,User>();
		
		for (User c : DataHandler.loadUsers()) {
			users.put(c.getId(), c);
		}
	}

	public User getById(Integer id) {
		return users.get(id);
	}
	
	public List<User> getFriends(Integer id){
		List<User> friends = new ArrayList<User>();
		List<Integer> friendIds = users.get(id).getFriends();
		for (Integer i : friendIds ) {
			friends.add(users.get(i));
		}
		return friends;
	}
	
	public List<Integer> getFriendsIds(Integer id){
		return users.get(id).getFriends();
	}
	public List<User> getBlocked(Integer id){
		List<User> blocks = new ArrayList<User>();
		List<Integer> blockIds = users.get(id).getBlocked();
		for (Integer i : blockIds ) {
			blocks.add(users.get(i));
		}
		return blocks;
		
	}
	
	public void addFriend(Integer receiver, Integer sender) throws JsonIOException, IOException {
		users.get(receiver).getFriends().add(sender);
		users.get(sender).getFriends().add(receiver);
		DataHandler.writeUsers(new ArrayList<User>(users.values()));
	}
	
	public User getByEmail(String email) {
		User retval = null;
		for (User u : this.users.values()) {
			if (email.equals(u.getEmail())) {
				retval = u;
				break;
			}
		}
		return retval;
	}
	
	public User getByUsername(String username) {
		User retval = null;
		for (User u : this.users.values()) {
			if (username.equals(u.getUsername())) {
				retval = u;
				break;
			}
		}
		return retval;
	}
	
	public void registerUser(User u) throws JsonIOException, IOException {
		u.setId(this.users.size() + 1);
		DataHandler.addUser(u);
		this.users.put(u.getId(), u);
	}
	
	public void removeFriend(Integer user, Integer friendId) throws JsonIOException, IOException {
		users.get(user).getFriends().remove(friendId);
		DataHandler.writeUsers(new ArrayList<User>(users.values()));	
	}
	
	public List<User> getMutualFriends(Integer user1, Integer user2){
		List<User> mutuals = new ArrayList<User>();
		for (User u : users.values()) {
			if (u.getFriends().contains(user1) && u.getFriends().contains(user2))
				mutuals.add(u);
		}
		return mutuals;
	}
	

	public void suspendUser(Integer usrid) throws JsonIOException, IOException {
		users.get(usrid).setSuspended(true);
		updateUsers();
}
	public List<User> getActiveUsers(){
		List<User> active = new ArrayList<User>();
		for (User u : users.values()) {
			if(u.getActivated() && !u.getSuspended()) {
				active.add(u);
			}
		}
		
		return active;
	}

	public void updateUsers() throws JsonIOException, IOException {
		DataHandler.writeUsers(new ArrayList<User>(users.values()));
	}
	
	public HashMap<Integer, String> getFullNames(){
		HashMap<Integer,String> map = new HashMap<Integer, String>();
		for (User u : users.values()) {
			map.put(u.getId(), u.getFullName());
		}
		return map;
	}
	
	public void togglePrivate(Integer id, Boolean priv) throws JsonIOException, IOException {
		users.get(id).setPrivated(priv);
		updateUsers();
	}
	
}
