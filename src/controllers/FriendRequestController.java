package controllers;

import java.util.List;

import com.google.gson.Gson;

import model.FriendRequest;
import model.enums.Role;
import services.FriendRequestService;
import spark.Spark;
import util.Authorizer;

public class FriendRequestController {
	
	private FriendRequestService frs;
	private Gson gson;
	private Authorizer auth;
	
	public FriendRequestController(FriendRequestService frs, Gson gson, Authorizer a) {
		this.frs = frs;
		this.gson = gson;
		this.auth = a;
		
		getFriendRequests();
		answerFriendRequest();
		sendFriendRequest();
		getFriendRequestCount();
	}

	@SuppressWarnings("unused")
	private class NewRequest {
		private Integer sender;
		private Integer receiver;
		
		public Integer getSender() {
			return sender;
		}
		public void setSender(Integer sender) {
			this.sender = sender;
		}
		public Integer getReceiver() {
			return receiver;
		}
		public void setReceiver(Integer receiver) {
			this.receiver = receiver;
		}
	}
	
	private void sendFriendRequest() {
		Spark.post("/friendrequest/send", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				NewRequest newRequest = gson.fromJson(req.body(), NewRequest.class);
				frs.sendRequest(newRequest.getSender(), newRequest.getReceiver());
				return "{\"message\": \"Request sent!\"}";
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Request failed!!\"}";
			}
		});		
	}
	
	@SuppressWarnings("unused")
	private class RequestAnswer{
		private Integer requestId;
		private Boolean accepted;
		public Integer getRequestId() {
			return requestId;
		}
		public void setRequestId(Integer requestId) {
			this.requestId = requestId;
		}
		public Boolean getAccepted() {
			return accepted;
		}
		public void setAccepted(Boolean accepted) {
			this.accepted = accepted;
		}
	}

	private void answerFriendRequest() {
		Spark.post("/friendrequest/answer", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				RequestAnswer ra = gson.fromJson(req.body(), RequestAnswer.class);
				if(ra.getAccepted()) {
					frs.acceptRequest(ra.getRequestId());
					return "{\"message\": \"Friend request accepted!\"}";
				} else {
					frs.denyRequest(ra.getRequestId());
					return "{\"message\": \"Friend request denied.\"}";
				}
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad Request\"}";
			}
			
		});
		
	}

	private void getFriendRequests() {
		Spark.get("/friendrequest/getall/:userid", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer userid = Integer.valueOf(req.params("userid"));
				List<FriendRequest> requests = frs.getPendingRequests(userid);
				return gson.toJson(requests);
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad Request\"}";
			}
			
		});		
	}
	
	private void getFriendRequestCount() {
		Spark.get("/friendrequest/count/:userid", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer userid = Integer.valueOf(req.params("userid"));
				List<FriendRequest> requests = frs.getPendingRequests(userid);
				return gson.toJson(requests.size());
			} catch (Exception e) {
				res.status(400);
				return "{\"message\": \"Bad Request\"}";
			}
			
		});		
	}
	

}
