package controllers;

import com.google.gson.Gson;

import model.Comment;
import model.enums.Role;
import services.CommentService;
import spark.Spark;
import util.Authorizer;

public class CommentController {
	
	private CommentService commentService;
	private Gson gson;
	private Authorizer auth;
	
	
	public CommentController(CommentService cs, Gson g, Authorizer a) {
		this.commentService = cs;
		
		this.gson = g;
		this.auth = a;
		postComment();
		deleteComment();
		editComment();
	}
	@SuppressWarnings("unused")
	private class CommentInfo {
		private Integer userid;
		private String comment;
		private Integer postid;
		public Integer getUserid() {
			return userid;
		}
		public void setUserid(Integer userid) {
			this.userid = userid;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
		public Integer getPostid() {
			return postid;
		}
		public void setPostid(Integer postid) {
			this.postid = postid;
		}
		
	}
	
	private void postComment() {
		Spark.post("/comment/post", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				String comment = req.body();
				CommentInfo ci = gson.fromJson(comment, CommentInfo.class);
				Comment c = commentService.postComment(ci.getUserid(), ci.getComment(), ci.getPostid());
				
				return gson.toJson(c);
			} catch (Exception e) {
				System.out.println(e.getStackTrace().toString());
				res.status(400);
				return "{\"message\":\"Posting failed!!\"}";
			}
		});
	}
	
	private void deleteComment() {
		Spark.delete("/delete/comment/:comid", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.ADMIN)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				Integer commentid = Integer.valueOf(req.params("comid"));
				commentService.deleteComment(commentid);
				return "{\"message\":\"Deletion successful!\"}";
			} catch (Exception e) {
				res.status(400);
				return "{\"message\":\"Deletion failed!!\"}";
			}
		});
	}
	@SuppressWarnings("unused")
	private class CommentEdit {
		 private String editedCommentText;
         private Integer commentId;
         private Integer userid;
		public String getEditedCommentText() {
			return editedCommentText;
		}
		public void setEditedCommentText(String editedCommentText) {
			this.editedCommentText = editedCommentText;
		}
		public Integer getCommentId() {
			return commentId;
		}
		public void setCommentId(Integer commentId) {
			this.commentId = commentId;
		}
		public Integer getUserid() {
			return userid;
		}
		public void setUserid(Integer userid) {
			this.userid = userid;
		}
	}
	
	private void editComment() {
		Spark.put("/comment/edit", (req, res) -> {
			res.type("application/json");
			
			if (!auth.checkJWTToken(req, Role.GUEST)) {
				res.status(401);
				return "{\"message\": \"Unauthorized\"}";
			}
			
			try {
				CommentEdit ce = gson.fromJson(req.body(), CommentEdit.class);
				commentService.editComment(ce.getEditedCommentText(), ce.getCommentId(), ce.getUserid());
				return "{\"message\":\"Edit successful!\"}";
			} catch (Exception e) {
				res.status(400);
				return "{\"message\":\"Edit failed!!\"}";
			}
		});
	}
}
